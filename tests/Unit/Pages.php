<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use \App\Product;
use \App\Category;
use \App\Offer;

class PagesTest extends TestCase
{
    /**
     * A home test
     *
     * @return void
     */
    public function testHome()
    {
        $product = factory(\App\Product::class)->create();

        $category = factory(\App\Category::class)->make();

        $category = factory(\App\Category::class)->make([
            'id' => 1,
            'title' => 'Product 1',
            'description' => 'Product 1 description',
            'url' => '/',
        ]);


        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * A products test
     *
     * @return void
     */
    public function testProducts()
    {
        $product = factory(\App\Product::class)->make([
            'id' => 1,
            'title' => 'Product 1',
            'description' => 'Product 1 description',
            'url' => '/',
        ]);

        $category = factory(\App\Category::class)->make([
            'id' => 1,
            'product_id' => 1,
        ]);

        $category = factory(\App\Category::class)->make([
            'id' => 1,
            'title' => 'Product 1',
            'description' => 'Product 1 description',
            'url' => '/',
        ]);

        $response = $this->get('product/index');

        $response->assertStatus(200);
    }

}

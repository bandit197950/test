<?php

use Faker\Generator as Faker;

$factory->define(App\Category::class, function (Faker $faker) {
    return [
        'id' => rand(1, 100),
        'parent' => null,
        'title' => $faker->name,
        'alias' => $faker->slug,
    ];
});

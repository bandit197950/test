<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'id' => rand(1, 100),
        'title' => $faker->name,
        'description' => $faker->name,
        'url' => $faker->url,
        'price' => rand(1, 100),
        'amount' => rand(1, 100)
    ];
});

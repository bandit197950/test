<?php

use Faker\Generator as Faker;

$factory->define(App\Offer::class, function (Faker $faker, int $id) {
    return [
        'id' => $id,
        'product_id' => function() {return factory(\App\Product::class)->create()->id; },
        'price' => rand(1, 100),
        'amount' => rand(1, 100),
        'sales' => rand(1, 100),
        'article' => (string)rand(1000000, 9000000),
    ];
});

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // товары products
        Schema::create('products', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->unique();              // айди товара
            $table->string('title')->index();                        // название товара
            $table->string('image')
                ->nullable()
                ->default(NULL);                                      // ссылка на изображение
            $table->text('description')
                ->nullable()
                ->default(NULL);                                      // описание товара
            $table->date('first_invoice')
                ->nullable()
                ->default(NULL);                                      // дата первой продажи товара
            $table->string('url');                                   // ссылка на товар на markethot.ru
            $table->double('price')->default(0.0);            // минимальная цена товара
            $table->unsignedInteger('amount')->default(0);    // количество всех вариаций
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

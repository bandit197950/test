<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // вариации товарам (например iphone 6 64gb черный // вариация товара iphone 6)
        Schema::create('offers', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->unique();          // айди вариации
            $table->unsignedBigInteger('product_id')->index();   // принадлежность к товару
            $table->foreign('product_id')->references('id')->on('products');
            $table->double('price')->default(0.0);         // цена вариации
            $table->unsignedInteger('amount')->default(0); // количество вариации товара на складе
            $table->unsignedInteger('sales')->default(0);  // единиц продано
            $table->string('article')
                ->nullable()
                ->default(NULL);                                  // артикул вариации
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}

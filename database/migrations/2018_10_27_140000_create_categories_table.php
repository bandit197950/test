<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // categories - категории товара
        Schema::create('categories', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->unique(); // айди категории
            $table->unsignedBigInteger('parent')
                ->nullable()
                ->default(NULL)
                ->index();                                     // родительская категория (у категорий есть иерархия)
            $table->string('title')
                ->nullable()
                ->default(NULL);                        // название категории
            $table->string('alias')
                ->nullable()
                ->default(NULL);                        // slug категории, можно использовать в качестве пути для ссылки на категорию
            $table->timestamps();
        });

        Schema::table('categories', function (Blueprint $table) {
            $table->foreign('parent')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}

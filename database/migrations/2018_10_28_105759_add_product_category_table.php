<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // product_category - определяет каким категориям принадлежат товары
        Schema::create('product_category', function (Blueprint $table) {
            $table->unsignedBigInteger('product_id')->index();  // айди товара
            $table->foreign('product_id')->references('id')->on('products');
            $table->unsignedBigInteger('category_id')->index(); // айди категории
            $table->foreign('category_id')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_category');
    }
}

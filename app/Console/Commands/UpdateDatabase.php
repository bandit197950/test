<?php

namespace App\Console\Commands;

use \Illuminate\Support\Facades\Schema;
use GuzzleHttp;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Console\Command;
use \DB;
use \Log;
use \Carbon\Carbon;
use \App\Product;
use \App\Offer;
use \App\Category;

class UpdateDatabase extends Command
{
    const JSON_URL = "https://markethot.ru/export/bestsp";
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'job:update_database';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update database from https://markethot.ru/export/bestsp';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    private function log_msg(String $msg, String $msgType = 'info')
    {
        Log::{$msgType}($msg . "\r\n");
        echo $msg . "\r\n";
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $r = -1;
        $this->log_msg('Start update database job');
        try {
            $products = $this->get_json_from_url(self::JSON_URL);
            if ($products !== NULL) {
                DB::beginTransaction();
                $this->update_db($products);
                DB::commit();
                // after database is updated, store categories into cache
                Category::cache_tree_list();
                Product::cache_best();
                $r = 1;
            }
        } catch (GuzzleHttp\Exception\RequestException $e) {
            $this->log_msg("Error occured in update database command: {$e->getMessage()}", 'error');
            DB::rollback();
            $r = 0;
        } catch(\Illuminate\Database\QueryException $e) {
            $this->log_msg("Error occured in update database command: {$e->getMessage()}", 'error');
            DB::rollback();
            $r = 0;
        } catch(Exception $e) {
            $this->log_msg("Error occured in update database command: {$e->getMessage()}", 'error');
            DB::rollback();
            $r = 0;
        }
        switch($r) {
            case 0:
                $this->log_msg("Error occured in database update job", 'error');
                break;
            case 1:
                $this->log_msg("Database updated successfully");
                break;
            default:
                $this->log_msg("Database is not updated", 'error');
        }
    }

    /**
     * Get json data from url
     * @param string $url
     * @return array
     */
    private function get_json_from_url(String $url)
    {
        $products = NULL;
        $client = new GuzzleHttp\Client();
        $res = $client->request('GET', $url);
        $status_code = $res->getStatusCode();
        if($status_code == '200') {
            $json_ret = json_decode($res->getBody(), TRUE);
            if(isset($json_ret['products'])) {
                $products = $json_ret['products'];
            }
        } else {
            $this->log_msg("Cannot get json from url: {$url} return code: {$status_code}", "error");
        }
        return $products;
    }

    /**
     * Proccess product list, filter only allowed column name, insert into tables
     * @param array $products
     */
    private function update_db(Array $products)
    {
        $batch_size = 500;
        $batch_categories = [];
        $batch_offers = [];
        $batch_products = [];
        $product_categories = [];

        $product_t = new Product();
        $offer_t = new Offer();
        $category_t = new Category();

        // get allowed columns
        $product_cols = Schema::getColumnListing($product_t->getTable());
        $offer_cols = Schema::getColumnListing($offer_t->getTable());
        $category_cols = Schema::getColumnListing($category_t->getTable());

        // clean tables
        DB::statement("SET foreign_key_checks=0");
        DB::table('product_category')->truncate();
        DB::table($offer_t->getTable())->truncate();
        DB::table($product_t->getTable())->truncate();
        DB::table($category_t->getTable())->truncate();
        DB::statement("SET foreign_key_checks=1");

        // set locale from config and get current timestamp
        setlocale(LC_TIME, config('app.locale'));
        $curr_dtm = Carbon::now()->format('Y-m-d H:i:s');

        // process products
        foreach($products as $product) {
            // process offers. remove not allowed columns, escapes string columns and replace null by 0 for numeric columns
            // add date fields
            $offers = array_map(function($item) use($product, $curr_dtm, $offer_cols) {
                array_walk($item, function(&$iitem, $key) {
                    if(is_string($iitem)) {
                        $iitem = escape_sql_string(nl_literal_to_br($iitem));
                    }
                    if($iitem === null && in_array($key, ['price', 'amount', 'sales'])) {
                        $iitem = 0;
                    }
                });
                $item['product_id'] = $product['id'];
                $item['created_at'] = $curr_dtm;
                $item['updated_at'] = $curr_dtm;
                return array_intersect_key($item, array_flip($offer_cols));
            }, $product['offers']);

            // process categories. remove not allowed columns, escapes string columns
            // add date fields. Also store info into many-to-many array product_categories
            $categories = array_map(function($item) use($product, $curr_dtm, $category_cols, &$product_categories) {
                array_walk($item, function(&$iitem, $key) {
                    if(is_string($iitem)) {
                        $iitem = escape_sql_string(nl_literal_to_br($iitem));
                    }
                });
                $item['created_at'] = $curr_dtm;
                $item['updated_at'] = $curr_dtm;
                $product_categories[] = ['product_id' => $product['id'], 'category_id' => $item['id']];
                return array_intersect_key($item, array_flip($category_cols));
            }, $product['categories']);

            // process product. remove not allowed columns, escapes string columns and replace null by 0 for numeric columns
            // add date fields
            $product['created_at'] = $curr_dtm;
            $product['updated_at'] = $curr_dtm;
            $product = array_intersect_key($product, array_flip($product_cols));

            array_walk($product, function(&$item, $key) {
                if(is_string($item)) {
                    $item = escape_sql_string(nl_literal_to_br($item));
                }
                if($item === null && in_array($key, ['price', 'amount'])) {
                    $item = 0;
                }
            });

            // store processed product, categories and offers into batch arrays
            $batch_products[] = $product;
            $batch_categories = array_merge($batch_categories, $categories);
            $batch_offers = array_merge($batch_offers, $offers);

            // products update by batch_size
            if(count($batch_products) >= $batch_size) {
                $this->mass_insert($product_t->getTable(), $batch_products);
                $this->mass_insert($offer_t->getTable(), $batch_offers);
                $batch_products = [];
                $batch_offers = [];
            }
        }
        // update tables
        $this->mass_insert($product_t->getTable(), $batch_products);
        $this->mass_insert($offer_t->getTable(), $batch_offers);

        $batch_categories = array_sort_by_parent(
                                    array_unique_by_field($batch_categories, 'id'),
                                    'id',
                                    'parent');
        $this->mass_insert($category_t->getTable(), $batch_categories);
        $this->mass_insert('product_category', $product_categories);
    }

    /**
     * Mass insert records into $tableName table
     * @param String $tableName
     * @param array $records
     */
    private function mass_insert(String $tableName, Array $records)
    {
        if(count($records) > 0) {
            DB::table($tableName)->insert($records);
        }
    }
}

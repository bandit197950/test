<?php
if ( !function_exists('escape_sql_string')) {
    function escape_sql_string($buf)
    {
        if (is_array($buf)) return array_map(__METHOD__, $buf);

        if (!empty($buf) && is_string($buf)) {
            return str_replace(array('\\', "\0", "\n", "\r", "\x1a"), array('\\\\', '\\0', '\\n', '\\r', '\\Z'), $buf);
        }

        return $buf;
    }
}

if ( !function_exists('nl_literal_tobr')) {
    function nl_literal_to_br($buf)
    {
        return preg_replace("/\r\n|\r|\n/",'<br/>',$buf);
    }
}
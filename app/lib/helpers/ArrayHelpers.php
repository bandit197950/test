<?php

if (!function_exists('array_unique_by_field')) {
    /**
     * Array unique by some field
     * @param array $list
     * @param String $field
     * @return array
     */
    function array_unique_by_field(Array $list, String $field)
    {
        $list_uniq_by_field = array_unique(array_column($list, $field));
        return array_intersect_key($list, $list_uniq_by_field);
    }
}

if (!function_exists('array_sort_by_parent')) {
    /**
     * Recursive sort array by parent field ($parentField)
     * @param array $list
     * @param String $idField
     * @param String $parentField
     * @param callback $levelSortCmpFunc <p>
     *  Compare function to sort one level items (having the same parent). If null (by default), no level items sorted
     * </p>
     * @param int $parentId
     * @return array
     */
    function array_sort_by_parent(Array $list, String $idField = 'id', String $parentField = 'parent_id', $levelSortCmpFunc = null, int $parentId = 0)
    {
        $parent_items = array_filter($list, function ($item) use ($parentId, $parentField) {
            return intval($item[$parentField]) === $parentId;
        });
        if($levelSortCmpFunc !== null) {
            usort($parent_items, $levelSortCmpFunc);
        }
        $sorted_list = $parent_items;
        foreach ($parent_items as $item) {
            $item_id = $item[$idField];
            // recursion
            $sorted_list = array_merge($sorted_list, array_sort_by_parent($list, $idField, $parentField, $levelSortCmpFunc, $item_id));
        }
        return $sorted_list;
    }
}

if (!function_exists('array_tree_list')) {
    /** Recursive make tree list
     * @param array $list
     * @param String $idField
     * @param String $parentField
     * @param int $parentId
     * @return array
     */
    function array_tree_list(Array $list, String $idField = 'id', String $parentField = 'parent_id', int $parentId = 0)
    {
        $result_list = [];
        $parent_items = array_filter($list, function ($item) use ($parentId, $parentField) {
            return intval($item[$parentField]) === $parentId;
        });
        foreach ($parent_items as $item) {
            // recursion
            $item['childs'] = array_tree_list($list, $idField, $parentField, $item[$idField]);
            $result_list[] = $item;
        }
        return $result_list;
    }
}
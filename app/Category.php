<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use App\Product;

class Category extends Model
{
    const CACHE_TREE_LIST_KEY = "category_hierarchy_list";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'parent', 'title', 'alias'
    ];
    /**
     * Return all products in current category
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_category', 'category_id', 'product_id');
    }

    /**
     * Get cached hierarchy of categories
     *
     * @return array
     */
    public static function get_cached_tree_list(): array
    {
        $tree_list = Cache::get(self::CACHE_TREE_LIST_KEY);
        if($tree_list === null) {
            $tree_list = self::cache_tree_list();
        }
        return $tree_list;
    }

    /**
     * Put hierarchy of categories to cache
     *
     * @return array
     */
    public static function cache_tree_list(): array
    {
        Cache::forget(self::CACHE_TREE_LIST_KEY);
        $list = Category::select('id','parent','title', 'alias')->get()->toArray();
        $list = array_sort_by_parent($list, 'id', 'parent', function($i1, $i2) {return $i1['title']<=>$i2['title'];});
        $list = array_tree_list($list, 'id', 'parent');
        Cache::forever(self::CACHE_TREE_LIST_KEY, $list);
        return $list;
    }
}

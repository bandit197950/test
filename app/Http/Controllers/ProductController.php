<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    const PRODUCTS_PER_PAGE = 10;

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $search = $request->input('product_search');
        $products = Product::whereRaw("title LIKE '%{$search}%' OR description LIKE '%{$search}%'")->paginate(self::PRODUCTS_PER_PAGE);
        return view('product.index', compact('products'));
    }

}

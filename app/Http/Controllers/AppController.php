<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;

class AppController extends Controller
{
    public function index()
    {
        $products = Product::get_cached_best();
        return view('home', compact('products'));
    }
}

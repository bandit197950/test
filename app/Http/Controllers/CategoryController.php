<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    const PRODUCTS_PER_PAGE = 10;
    /**
     * Display a listing of the products.
     *
     * @return \Illuminate\Http\Response
     */
    public function products(Request $request, String $alias)
    {
        $category = Category::where('alias', $alias)->first();
        if(empty($category)) {
            abort(404);
        } else {
            $products = $category->products()->orderBy('title')->paginate(self::PRODUCTS_PER_PAGE);
            return view('category.products', compact('category', 'products'));
        }
    }

}

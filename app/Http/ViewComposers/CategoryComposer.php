<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Category;

class CategoryComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $categories_tree_list;

    /**
     * Create a new category composer.
     *
     * @return void
     */
    public function __construct()
    {
        $this->categories_tree_list = Category::get_cached_tree_list();
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('categories_tree_list', $this->categories_tree_list);
    }
}
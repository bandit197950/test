<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use DB;
use App\Category;

class Product extends Model
{
    const POPULAR_LIMIT = 20;
    const GROUP_CONCAT_DELIM = '#delim#';
    const CACHE_BEST_KEY = "best_products";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'title', 'image', 'description', 'first_invoice', 'url', 'price', 'amount'
    ];

    /**
     * Calculate 20 most popular products
     * @param $query
     * @return mixed
     */
    public function scopePopular($query)
    {
        $sales_sql = Offer::selectRaw("product_id, SUM(sales) as total_sales")->groupBy("product_id")->toSql();
        return $query->selectRaw('sales.total_sales')
            ->join(DB::raw("({$sales_sql}) sales"), 'products.id', '=', 'sales.product_id')->orderBy('sales.total_sales', 'DESC')->limit(self::POPULAR_LIMIT);
    }

    public function scopeWithCategories($query)
    {
        $delim = self::GROUP_CONCAT_DELIM;
        $categories_sql = Category::selectRaw("
                                                product_category.product_id,
                                                GROUP_CONCAT(categories.title SEPARATOR '{$delim}') as categories_titles,
                                                GROUP_CONCAT(categories.alias SEPARATOR '{$delim}') as categories_aliases")
                            ->join('product_category', 'id', '=', 'product_category.category_id')
                            ->groupBy("product_id")->toSql();
        return $query->selectRaw('product_categories.categories_titles, product_categories.categories_aliases')
            ->leftJoin(DB::raw("({$categories_sql}) product_categories"), 'products.id', '=', 'product_categories.product_id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function offers()
    {
        return $this->hasMany('offers');
    }

    /**
     * Get best products from cache
     *
     * @return array
     */
    public static function get_cached_best(): array
    {
        $best_list = Cache::get(self::CACHE_BEST_KEY);
        if($best_list === null) {
            self::cache_tree_list();
            $best_list = Cache::get(self::CACHE_BEST_KEY);
        }
        return $best_list;
    }

    /**
     * Cache best products with categories
     *
     * @return array
     */
    public static function cache_best(): array
    {
        Cache::forget(self::CACHE_BEST_KEY);
        $list = Product::select('products.*')->popular()->withCategories()->get()->toArray();
        array_walk(
            $list,
            function(&$item) {
                $item['categories_titles'] = explode(self::GROUP_CONCAT_DELIM, $item['categories_titles']);
                $item['categories_aliases'] = explode(self::GROUP_CONCAT_DELIM, $item['categories_aliases']);
            }
        );
        Cache::forever(self::CACHE_BEST_KEY, $list);
        return $list;
    }

}

@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            {{  __("pages.product.caption") }}
        </div>
    </div>
    @include('product.partials.search')
    {{ $products->links() }}
    @include('product.partials.list', ['products'=>$products])
    {{ $products->links() }}
@endsection
<div class="card product">
    <div class="card-header">
        {{  $product['title'] }}
    </div>
    <div class="card-body">
        <div>
            <a href="{{  $product['url'] }}">
                <img src="{{ $product['image'] }}" alt="{{ $product['title'] }}">
            </a>
        </div>
        <div>
            <label class="font-weight-bold">{{ __("pages.product.description") }}:</label>
            <span>{!! $product['description'] !!}</span>
        </div>
        @if(isset($with_categories) && $with_categories === true)
            @php
                $cat_titles  = $product['categories_titles'];
                $cat_aliases = $product['categories_aliases'];
                $categories = array_combine($cat_aliases, $cat_titles);
                asort($categories);
            @endphp
            <div>
                <label class="font-weight-bold">{{ __("pages.product.categories") }}:</label>
                <ul>
                    @foreach($categories as $alias => $title)
                        <li><a href="{{ route('category.products', ['alias' => $alias]) }}">{{ $title }}</a></li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div>
            <label class="font-weight-bold">{{ __("pages.product.price") }}:</label>
            <span>{{ $product['price'] }}</span>
        </div>
    </div>
</div>
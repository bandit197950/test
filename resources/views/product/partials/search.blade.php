<form method="get" action="{{ route('product.index') }}">
    @csrf

    <input type="hidden" name="_method" value="get" />

    <div class="input-group mb-3 product-search">
        <input type="text" class="form-control" name="product_search" id="product_search" placeholder="{{ __('pages.product.search') }}" aria-label="Recipient's username" aria-describedby="basic-addon2">
        <div class="input-group-append">
            <button class="btn btn-outline-secondary">{{ __('pages.product.search') }}</button>
        </div>
    </div>
</form>
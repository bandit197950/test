@foreach($products as $product)
    @include('product.partials.item', ['product' => $product, 'with_categories' => isset($with_categories) ? $with_categories : false])
@endforeach
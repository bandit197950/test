@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            {{  __("pages.home.most_popular_products.caption") }}
        </div>
    </div>
    @include('product.partials.search')
    @include('product.partials.list', ['products' => $products, 'with_categories' => true])
@endsection
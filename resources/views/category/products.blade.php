@extends('layouts.app')

@section('content')
    {{ $products->links() }}
    <div class="card">
        <div class="card-header">
            {{  __("pages.category_products.caption") }}: {{ $category->title }}
        </div>
    </div>
    @include('product.partials.list', ['products'=>$products])
    {{ $products->links() }}
@endsection
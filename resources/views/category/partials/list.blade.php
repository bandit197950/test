<ul>
    @foreach($categories as $category)
        <li><a href="{{ route("category.products", ['alias'=>$category['alias']]) }}">{{ $category['title'] }}</a></li>
        {{-- Recursion --}}
        @includeWhen(!empty($category['childs']), 'category.partials.list', ['categories' => $category['childs']])
    @endforeach
</ul>
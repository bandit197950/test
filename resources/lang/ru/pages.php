<?php

return [
    "home.caption" => "Главная",
    "home.most_popular_products.caption" => "Популярные товары",
    "product.caption" => "Товары",
    "product.title" => "Наименование",
    "product.description" => "Описание",
    "product.categories" => "Категории",
    "product.url" => "Ссылка",
    "product.price" => "Цена",
    "product.amount" => "Кол-во вариаций",
    "product.search" => "Поиск",
    "category_products.caption" => 'Категория',
];

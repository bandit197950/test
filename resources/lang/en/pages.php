<?php

return [
    "home.caption" => "Home",
    "home.most_popular_products.caption" => "Most popular products",
    "product.caption" => "Products",
    "product.title" => "Title",
    "product.description" => "Description",
    "product.url" => "Url",
    "product.price" => "Price",
    "product.amount" => "Variations",
    "product.search" => "Search",
    "category_products.caption" => 'Category'
];
